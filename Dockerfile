FROM $DOCKERIMAGE

WORKDIR /app
COPY /publish .

EXPOSE 5000/tcp

ENV ASPNETCORE_URLS "http://*:5000"
ENV ASPNETCORE_ENVIRONMENT "$ENVIRONMENT"
ENV TEST "$ENTRYPOINT"
# Avoid SQL Globalization Error
RUN apk add icu-libs

RUN apk add git libwbclient
RUN git clone https://github.com/mikeTWC1984/gssntlm
WORKDIR gssntlm
RUN mkdir -p /usr/local/lib/gssntlmssp  /usr/etc/gss/mech.d
RUN cp gssntlmssp.so /usr/local/lib/gssntlmssp/
RUN cp mech.ntlmssp.conf  /usr/etc/gss/mech.d

ARG TZ='Europe/Berlin'

ENV DEFAULT_TZ ${TZ}

RUN apk upgrade --update \
  && apk add -U tzdata \
  && cp /usr/share/zoneinfo/${DEFAULT_TZ} /etc/localtime


WORKDIR /app

#Remove build
#RUN rm -R gss-ntlmssp

ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=false

CMD dotnet /app/$ENTRYPOINT
