namespace SpectruMeter.Apis.ContractApi.Models
{
    public class ContractCreateResponseDto
    {
        public ContractDto Contract { get; set; }
    }
}
