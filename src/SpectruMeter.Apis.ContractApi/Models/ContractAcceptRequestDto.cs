namespace SpectruMeter.Apis.ContractApi.Models
{
    public class ContractAcceptRequestDto
    {
        public DateTimeOffset? ValidFrom { get; set; }
        public DateTimeOffset? ValidTo { get; set; }
        public string MinTimeResolution { get; set; }
        public int MinAggregation { get; set; }
        public int DaysDelay { get; set; }
    }
}
