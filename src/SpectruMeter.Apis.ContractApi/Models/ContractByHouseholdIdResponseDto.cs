namespace SpectruMeter.Apis.ContractApi.Models
{
    public class ContractByHouseholdIdResponseDto
    {
        public ContractDto[] Contracts { get; set; }
    }
}
