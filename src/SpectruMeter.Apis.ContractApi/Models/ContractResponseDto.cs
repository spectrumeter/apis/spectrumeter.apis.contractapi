namespace SpectruMeter.Apis.ContractApi.Models
{
    public class ContractResponseDto
    {
        public ContractDto Contract { get; set; }
    }
}
