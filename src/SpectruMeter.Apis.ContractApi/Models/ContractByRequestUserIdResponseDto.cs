namespace SpectruMeter.Apis.ContractApi.Models
{
    public class ContractByRequestUserIdResponseDto
    {
        public ContractDto[] Contracts { get; set; }
    }
}
