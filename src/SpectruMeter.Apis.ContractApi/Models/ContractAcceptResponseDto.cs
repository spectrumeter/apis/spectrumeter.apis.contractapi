namespace SpectruMeter.Apis.ContractApi.Models
{
    public class ContractAcceptResponseDto
    {
        public ContractDto Contract { get; set; }
    }
}
