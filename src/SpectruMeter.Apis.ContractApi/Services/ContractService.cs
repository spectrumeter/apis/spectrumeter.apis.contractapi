using Mapster;
using Microsoft.EntityFrameworkCore;
using SpectruMeter.Apis.ContractApi.DAL.Interfaces;
using SpectruMeter.Apis.ContractApi.DAL.Models;
using SpectruMeter.Apis.ContractApi.Models;
using SpectruMeter.Apis.ContractApi.Services.Interfaces;

namespace SpectruMeter.Apis.ContractApi.Services
{
    public class ContractService : IContractService
    {
        private readonly ILogger<ContractService> _logger;
        private readonly IContractRepository _contractRepository;

        public ContractService(
            ILogger<ContractService> logger,
            IContractRepository contractRepository
        )
        {
            this._logger = logger;
            this._contractRepository = contractRepository;
        }

        public async Task<ContractDto> AcceptAsync(
            Guid contractId,
            ContractAcceptRequestDto data,
            CancellationToken cancellationToken = default
        )
        {
            await this._contractRepository.UpdateAsync(
                o => o.Id == contractId,
                o =>
                {
                    o.AcceptedAt = DateTime.UtcNow;
                    o.ValidFrom = data.ValidFrom;
                    o.ValidTo = data.ValidTo;
                    o.DaysDelay = data.DaysDelay;
                    o.MinAggregation = data.MinAggregation;
                    o.MinTimeResolution = data.MinTimeResolution;
                    o.DeclinedAt = null;
                },
                cancellationToken
            );

            return await this.GetAsync(contractId, cancellationToken);
        }

        public async Task<ContractDto> CreateAsync(
            string dataRequestUserId,
            Guid householdId,
            CancellationToken cancellationToken = default
        )
        {
            var existingContract = await this._contractRepository
                .GetQuery(
                    o => o.DataRequesterUserId == dataRequestUserId && o.HouseholdId == householdId
                )
                .FirstOrDefaultAsync();

            if (existingContract == null)
            {
                var contract = new Contract
                {
                    AcceptedAt = null,
                    DataRequesterUserId = dataRequestUserId,
                    HouseholdId = householdId,
                    DaysDelay = 0,
                    MinAggregation = 0,
                    MinTimeResolution = "minute",
                    DeclinedAt = null
                };

                await this._contractRepository.AddAsync(contract, cancellationToken);

                return contract.Adapt<ContractDto>();
            }

            return existingContract.Adapt<ContractDto>();
        }

        public async Task<ContractDto> DeclineAsync(
            Guid contractId,
            CancellationToken cancellationToken = default
        )
        {
            await this._contractRepository.UpdateAsync(
                o => o.Id == contractId,
                o =>
                {
                    o.AcceptedAt = null;
                    o.DeclinedAt = DateTime.UtcNow;
                },
                cancellationToken
            );

            return await this.GetAsync(contractId, cancellationToken);
        }

        public async Task DeleteAsync(
            Guid contractId,
            CancellationToken cancellationToken = default
        )
        {
            await this._contractRepository.DeleteAsync(contractId, cancellationToken);
        }

        public async Task<ContractDto> GetAsync(
            Guid contractId,
            CancellationToken cancellationToken = default
        )
        {
            return (
                await this._contractRepository
                    .GetQuery(o => o.Id == contractId)
                    .FirstOrDefaultAsync()
            ).Adapt<ContractDto>();
        }

        public async Task<ContractDto[]> GetByDataRequestUserIdAsync(
            string dataRequestUserId,
            CancellationToken cancellationToken = default
        )
        {
            return (
                await (
                    this._contractRepository.GetQuery(
                        o => o.DataRequesterUserId == dataRequestUserId && o.AcceptedAt != null
                    )
                ).ToListAsync()
            ).Adapt<ContractDto[]>();
        }

        public async Task<ContractDto[]> GetByHouseholdIdAsync(
            Guid householdId,
            CancellationToken cancellationToken = default
        )
        {
            return (
                await (
                    this._contractRepository.GetQuery(o => o.HouseholdId == householdId)
                ).ToListAsync()
            ).Adapt<ContractDto[]>();
        }
    }
}
