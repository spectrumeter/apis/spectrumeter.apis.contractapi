using SpectruMeter.Apis.ContractApi.Models;

namespace SpectruMeter.Apis.ContractApi.Services.Interfaces
{
    public interface IContractService
    {
        Task<ContractDto> CreateAsync(
            string dataRequestUserId,
            Guid householdId,
            CancellationToken cancellationToken = default
        );

        Task<ContractDto> AcceptAsync(
            Guid contractId,
            ContractAcceptRequestDto data,
            CancellationToken cancellationToken = default
        );

        Task<ContractDto> DeclineAsync(
            Guid contractId,
            CancellationToken cancellationToken = default
        );

        Task DeleteAsync(Guid contractId, CancellationToken cancellationToken = default);

        Task<ContractDto> GetAsync(Guid contractId, CancellationToken cancellationToken = default);

        Task<ContractDto[]> GetByDataRequestUserIdAsync(
            string dataRequestUserId,
            CancellationToken cancellationToken = default
        );

        Task<ContractDto[]> GetByHouseholdIdAsync(
            Guid householdId,
            CancellationToken cancellationToken = default
        );
    }
}
