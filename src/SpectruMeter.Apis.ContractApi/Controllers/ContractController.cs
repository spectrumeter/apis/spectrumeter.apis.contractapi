using Microsoft.AspNetCore.Mvc;
using SpectruMeter.Apis.ContractApi.Models;
using SpectruMeter.Apis.ContractApi.Services.Interfaces;

namespace SpectruMeter.Apis.ContractApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ContractController : ControllerBase
    {
        private readonly ILogger<ContractController> _logger;
        private readonly IContractService _contractService;

        public ContractController(
            ILogger<ContractController> logger,
            IContractService contractService
        )
        {
            this._logger = logger;
            this._contractService = contractService;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [Route("User/{dataRequestUserId}/Household/{householdId}")]
        public async Task<ActionResult<ContractCreateResponseDto>> Create(
            [FromRoute] string dataRequestUserId,
            [FromRoute] Guid householdId
        )
        {
            var contract = await this._contractService.CreateAsync(dataRequestUserId, householdId);
            return this.StatusCode(
                StatusCodes.Status201Created,
                new ContractCreateResponseDto { Contract = contract }
            );
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Route("{contractId}/Accept")]
        public async Task<ActionResult<ContractAcceptResponseDto>> Accept(
            [FromRoute] Guid contractId,
            [FromBody] ContractAcceptRequestDto data
        )
        {
            var contract = await this._contractService.AcceptAsync(contractId, data);
            return this.StatusCode(
                StatusCodes.Status200OK,
                new ContractAcceptResponseDto { Contract = contract }
            );
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Route("{contractId}/Decline")]
        public async Task<ActionResult<ContractAcceptResponseDto>> Decline(
            [FromRoute] Guid contractId
        )
        {
            var contract = await this._contractService.DeclineAsync(contractId);
            return this.StatusCode(
                StatusCodes.Status200OK,
                new ContractAcceptResponseDto { Contract = contract }
            );
        }

        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [Route("{contractId}")]
        public async Task<ActionResult> Delete([FromRoute] Guid contractId)
        {
            await this._contractService.DeleteAsync(contractId);
            return this.StatusCode(StatusCodes.Status204NoContent);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Route("{contractId}")]
        public async Task<ActionResult<ContractResponseDto>> Get([FromRoute] Guid contractId)
        {
            var contract = await this._contractService.GetAsync(contractId);
            return this.StatusCode(
                StatusCodes.Status200OK,
                new ContractResponseDto { Contract = contract }
            );
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Route("User/{dataRequestUserId}")]
        public async Task<ActionResult<ContractByRequestUserIdResponseDto>> GetByDataRequestUserId(
            [FromRoute] string dataRequestUserId
        )
        {
            var contracts = await this._contractService.GetByDataRequestUserIdAsync(
                dataRequestUserId
            );
            return this.StatusCode(
                StatusCodes.Status200OK,
                new ContractByRequestUserIdResponseDto { Contracts = contracts }
            );
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Route("Household/{householdId}")]
        public async Task<ActionResult<ContractByHouseholdIdResponseDto>> GetByHouseholdId(
            [FromRoute] Guid householdId
        )
        {
            var contracts = await this._contractService.GetByHouseholdIdAsync(householdId);
            return this.StatusCode(
                StatusCodes.Status200OK,
                new ContractByHouseholdIdResponseDto { Contracts = contracts }
            );
        }
    }
}
