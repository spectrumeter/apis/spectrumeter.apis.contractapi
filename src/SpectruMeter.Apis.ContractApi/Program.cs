using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.EntityFrameworkCore;
using SpectruMeter.Apis.ContractApi.DAL;
using SpectruMeter.Apis.ContractApi.DAL.Interfaces;
using SpectruMeter.Apis.ContractApi.Services;
using SpectruMeter.Apis.ContractApi.Services.Interfaces;

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration;

AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

// Add Database
builder.Services.AddDbContext<DatabaseContext>(o =>
{
    o.UseNpgsql(configuration["ConnectionStrings:Default"])
        .LogTo(Console.WriteLine, LogLevel.Warning)
        .EnableDetailedErrors();
});

// Add services to the container.
builder.Services.AddScoped<IContractService, ContractService>();

builder.Services.AddScoped<IContractRepository, SqlContractRepository>();

builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(configuration =>
{
    configuration.CustomOperationIds(description =>
    {
        var actionDescriptor = description.ActionDescriptor as ControllerActionDescriptor;
        return $"{actionDescriptor.ControllerName}{actionDescriptor.ActionName}";
    });
});

var app = builder.Build();
var logger = app.Services.GetService<ILogger<Program>>();

UpdateDatabaseAsync(logger, app).GetAwaiter().GetResult();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

app.Run();

async Task UpdateDatabaseAsync(ILogger<Program> logger, IApplicationBuilder app)
{
    var retryCount = 60;

    for (var i = 0; i < retryCount; i++)
    {
        using (
            var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope()
        )
        {
            using (var context = serviceScope.ServiceProvider.GetService<DatabaseContext>())
            {
                try
                {
                    context.Database.Migrate();
                    break;
                }
                catch (Exception exception)
                {
                    logger.LogError(
                        exception,
                        $"{nameof(UpdateDatabaseAsync)} - Cannot execute database migrate, retry: {i}"
                    );
                    await Task.Delay(1000);
                }
            }
        }
    }
}
