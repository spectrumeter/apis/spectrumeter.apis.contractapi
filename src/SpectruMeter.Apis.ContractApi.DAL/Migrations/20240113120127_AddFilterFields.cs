﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SpectruMeter.Apis.ContractApi.DAL.Migrations
{
    /// <inheritdoc />
    public partial class AddFilterFields : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DaysDelay",
                table: "Contracts",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MinAggregation",
                table: "Contracts",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "MinTimeResolution",
                table: "Contracts",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "ValidFrom",
                table: "Contracts",
                type: "timestamp with time zone",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "ValidTo",
                table: "Contracts",
                type: "timestamp with time zone",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DaysDelay",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "MinAggregation",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "MinTimeResolution",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "ValidFrom",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "ValidTo",
                table: "Contracts");
        }
    }
}
