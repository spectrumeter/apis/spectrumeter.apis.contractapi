﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SpectruMeter.Apis.ContractApi.DAL.Migrations
{
    /// <inheritdoc />
    public partial class Init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Contracts",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    HouseholdId = table.Column<Guid>(type: "uuid", nullable: false),
                    DataRequesterUserId = table.Column<string>(type: "text", nullable: false),
                    AcceptedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contracts", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Contracts_AcceptedAt",
                table: "Contracts",
                column: "AcceptedAt");

            migrationBuilder.CreateIndex(
                name: "IX_Contracts_DataRequesterUserId",
                table: "Contracts",
                column: "DataRequesterUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Contracts_HouseholdId",
                table: "Contracts",
                column: "HouseholdId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Contracts");
        }
    }
}
