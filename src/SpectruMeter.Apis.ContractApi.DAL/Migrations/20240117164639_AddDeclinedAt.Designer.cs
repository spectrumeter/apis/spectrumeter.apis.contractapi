﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using SpectruMeter.Apis.ContractApi.DAL;

#nullable disable

namespace SpectruMeter.Apis.ContractApi.DAL.Migrations
{
    [DbContext(typeof(DatabaseContext))]
    [Migration("20240117164639_AddDeclinedAt")]
    partial class AddDeclinedAt
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.11")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            NpgsqlModelBuilderExtensions.UseIdentityByDefaultColumns(modelBuilder);

            modelBuilder.Entity("SpectruMeter.Apis.ContractApi.DAL.Models.Contract", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<DateTimeOffset?>("AcceptedAt")
                        .HasColumnType("timestamp with time zone");

                    b.Property<string>("DataRequesterUserId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<int>("DaysDelay")
                        .HasColumnType("integer");

                    b.Property<DateTimeOffset?>("DeclinedAt")
                        .HasColumnType("timestamp with time zone");

                    b.Property<Guid>("HouseholdId")
                        .HasColumnType("uuid");

                    b.Property<int>("MinAggregation")
                        .HasColumnType("integer");

                    b.Property<string>("MinTimeResolution")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<DateTimeOffset?>("ValidFrom")
                        .HasColumnType("timestamp with time zone");

                    b.Property<DateTimeOffset?>("ValidTo")
                        .HasColumnType("timestamp with time zone");

                    b.HasKey("Id");

                    b.HasIndex("AcceptedAt");

                    b.HasIndex("DataRequesterUserId");

                    b.HasIndex("HouseholdId");

                    b.ToTable("Contracts");
                });
#pragma warning restore 612, 618
        }
    }
}
