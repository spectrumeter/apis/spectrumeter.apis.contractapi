﻿using SpectruMeter.Apis.ContractApi.DAL.Models;
using System.Linq.Expressions;

namespace SpectruMeter.Apis.ContractApi.DAL.Interfaces
{
    public interface IContractRepository
    {
        Task<Contract> AddAsync(Contract contract, CancellationToken cancellationToken = default);

        IQueryable<Contract> GetQuery(Expression<Func<Contract, bool>>? predicate = default);

        Task<bool> UpdateAsync(
            Expression<Func<Contract, bool>> predicate,
            Action<Contract> updateAction,
            CancellationToken cancellationToken = default
        );

        Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken = default);
    }
}
