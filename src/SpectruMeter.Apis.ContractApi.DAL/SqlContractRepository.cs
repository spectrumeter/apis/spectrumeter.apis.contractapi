using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SpectruMeter.Apis.ContractApi.DAL.Interfaces;
using SpectruMeter.Apis.ContractApi.DAL.Models;
using System.Linq.Expressions;

namespace SpectruMeter.Apis.ContractApi.DAL
{
    public class SqlContractRepository : IContractRepository
    {
        private readonly ILogger<SqlContractRepository> _logger;
        private readonly DatabaseContext _databaseContext;

        public SqlContractRepository(
            ILogger<SqlContractRepository> logger,
            DatabaseContext databaseContext
        )
        {
            this._logger = logger;
            this._databaseContext = databaseContext;
        }

        public async Task<Contract> AddAsync(
            Contract contract,
            CancellationToken cancellationToken = default
        )
        {
            await this._databaseContext.AddAsync(contract, cancellationToken);
            await this._databaseContext.SaveChangesAsync(cancellationToken);

            return contract;
        }

        public async Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken = default)
        {
            var contract = await this._databaseContext.Contracts.FirstOrDefaultAsync(
                o => o.Id == id,
                cancellationToken
            );
            if (contract == null)
            {
                return false;
            }

            this._databaseContext.Contracts.Remove(contract);
            await this._databaseContext.SaveChangesAsync(cancellationToken);

            return true;
        }

        public IQueryable<Contract> GetQuery(Expression<Func<Contract, bool>>? predicate = null)
        {
            if (predicate == null)
            {
                predicate = o => true;
            }

            return this._databaseContext.Contracts.Where(predicate);
        }

        public async Task<bool> UpdateAsync(
            Expression<Func<Contract, bool>> predicate,
            Action<Contract> updateAction,
            CancellationToken cancellationToken = default
        )
        {
            var facility = await this._databaseContext.Contracts
                .Where(predicate)
                .ToArrayAsync(cancellationToken);

            foreach (var item in facility)
            {
                updateAction(item);
            }

            await this._databaseContext.SaveChangesAsync(cancellationToken);

            return true;
        }
    }
}
