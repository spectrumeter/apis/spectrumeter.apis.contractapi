using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SpectruMeter.Apis.ContractApi.DAL.Models
{
    public class Contract
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public Guid HouseholdId { get; set; }
        public string DataRequesterUserId { get; set; }
        public DateTimeOffset? AcceptedAt { get; set; }
        public DateTimeOffset? DeclinedAt { get; set; }
        public DateTimeOffset? ValidFrom { get; set; }
        public DateTimeOffset? ValidTo { get; set; }
        public string MinTimeResolution { get; set; }
        public int MinAggregation { get; set; }
        public int DaysDelay { get; set; }
    }
}
