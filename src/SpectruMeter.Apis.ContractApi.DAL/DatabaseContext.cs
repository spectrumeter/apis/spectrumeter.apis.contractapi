using Microsoft.EntityFrameworkCore;
using SpectruMeter.Apis.ContractApi.DAL.Models;

namespace SpectruMeter.Apis.ContractApi.DAL
{
    public class DatabaseContext : DbContext
    {
        private readonly DbContextOptions<DatabaseContext> _options;
        public DbSet<Contract> Contracts { get; set; }

        public DatabaseContext() { }

        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        {
            this._options = options;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql(
                    "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;Trust Server Certificate=False;Application Intent=ReadWrite;Multi Subnet Failover=False"
                );
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Contract>().HasIndex(o => o.DataRequesterUserId);
            builder.Entity<Contract>().HasIndex(o => o.HouseholdId);
            builder.Entity<Contract>().HasIndex(o => o.AcceptedAt);
        }
    }
}
