# SpectruMeter.Apis.ContractApi
The SpectruMeter.Apis.ContractApi handles all requests for a contract. Like sending a contract request, accepting a contract, etc.

## Requirements
The run the SpectruMeter.Apis.ContractApi you need:
- PostgreSQL

## Configuration
In appsettings.json

```json
  "ConnectionStrings": {
    "Default": "Host=localhost; Database=contractdapi; Username=postgres; Password=password"
  },
```
## Docker
```
docker run -e ConnectionStrings__Default="Host=localhost; Database=contractdapi; Username=postgres; Password=password" -p 5001:5000 registry.gitlab.com/spectrumeter/apis/spectrumeter.apis.contractapi:main-latest
```

## Open Api
If you run SpectruMeter.Apis.ContractApi in debug mode a swagger will be exposed to http://address:port/swagger
